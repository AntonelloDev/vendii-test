export const addClasses = (classes) => classes.join(' ');
const schema = ['id', 'full_name', 'description', 'url', 'archive_url'];
/**
 *
 * @param {object} obj
 * @returns {object} new object with only the selected props
 */
export const selectInSchema = (obj) => {
  const newObj = {};
  for (let key in obj) {
    if (schema.includes(key)) {
      newObj[key] = obj[key];
    }
  }
  return newObj;
};
/**
 *
 * @param {number} time
 * @returns {Promise}
 */
export const delay = (time) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
/**
 *
 * @param {number} start
 * @param {number} end
 * @returns {array}
 */
export const range = (start, end) => {
  const nums = [];
  for (let i = start; i <= end; i++) {
    nums.push(i);
  }
  return nums;
};
/**
 * @description
 * @param {string} link
 * @return {string} the next since param
 * Get the next since params from git hub header link
 */
export const getNextSinceParam = (link) => {
  if (link) {
    return link?.split('?since=')[1]?.split('>')[0];
  }
};
/**
 *
 * @param {number} num
 * @returns number
 */
export function findFirstElementIndex(num) {
  if (num === 1) return 1;
  const numStr = String(num);
  if (numStr.length < 2) return num * 10 - 9;
  return Number(
    numStr.split('')[numStr.split('').length - 2] +
      numStr.split('')[numStr.split('').length - 1]
  );
}
/**
 * @param {array} array
 * @param {number} page
 * @param {number} MAX_PER_PAGE
 * @returns {array}
 */
export function paginate(array, page, MAX_PER_PAGE) {
  const pageData = [];
  let first = page === 1 ? page : page * MAX_PER_PAGE - 10;
  if (first >= 100) {
    first = findFirstElementIndex(first);
  }
  // const first = findFirstElementIndex(page);
  const last = first + Number(MAX_PER_PAGE);
  for (let i = first; i < last; i++) {
    pageData.push(selectInSchema(array[i]));
  }
  return pageData;
}
