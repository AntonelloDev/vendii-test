import axios from 'axios';
const headers = {
  'Content-Type': 'application/vnd.github.v3+json',
};
const client = axios.create({
  baseURL: process.env.REACT_APP_GIT_API,
  timeout: 1000,
  headers,
});
const API = {
  /**
   *
   * @param {string} url
   * @param {object} params
   * @param {number} params.since
   * @returns {Promise}
   */
  get: (url, { since = 1 }) =>
    client.get(url, {
      params: {
        since,
      },
    }),
};
export default API;
