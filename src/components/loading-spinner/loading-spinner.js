import './loading-spinner.scss';
const LoadingSpinner = () => {
  return (
    <div className="loading-container">
      <div className="lds-hourglass"></div>
    </div>
  );
};
export default LoadingSpinner;
