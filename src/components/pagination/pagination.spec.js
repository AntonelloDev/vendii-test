import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import Pagination from './pagination';

beforeEach(() => {
  const changePage = jest.fn();
  render(
    <Pagination
      changePage={changePage}
      maxPages={10}
      pagesCount={30}
      currentPage={1}
      end={10}
    />
  );
});
describe('testing click events', () => {
  test('Button page 1 should contain the class selected at mount', async () => {
    expect(true).toBe(true);
    // const firstElement = screen.getAllByTestId('page-button-element');
    // fireEvent.click(screen.getByTestId('next-button'));
  });
});
