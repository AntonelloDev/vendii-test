import Button from '../button/button';
import DoubleArrowLeft from '../icons/DoubleArrowLeft';
import DoubleArrowRight from '../icons/DoubleArrowRight';
import PropTypes from 'prop-types';
import './pagination.scss';
import { range } from '../../utils';
const Pagination = ({ currentPage, end, changePage, pagesCount }) => {
  const start = end - pagesCount + 1;
  const pageNumbers = range(start, end);
  const renderPageNumbers = pageNumbers.map((number) => {
    return (
      <li
        data-testid="page-button-element"
        style={{ fontWeight: currentPage === number ? 'bold' : 'unset' }}
        key={number}
        id={number}
        onClick={() => changePage(number, end, null)}
      >
        <Button
          text={number}
          handleClick={changePage}
          selected={currentPage === number}
        ></Button>
      </li>
    );
  });
  const nextPage = () => {
    if (end - (currentPage + 1) < 1) {
      changePage(currentPage + 1, Number(end) + pagesCount, 'r');
    }
  };
  const prevPage = () => {
    if (currentPage - 1 > 0) {
      if (currentPage === start) {
        changePage(currentPage - pagesCount, Number(end) - pagesCount, 'l');
      }
    }
  };
  return (
    <div className="pagination">
      <ul id="page-numbers">
        <Button
          data-testid="prev-button"
          handleClick={prevPage}
          text={<DoubleArrowLeft fill="white" />}
          disabled={currentPage > start || currentPage === 1}
        />
        {renderPageNumbers}
        <Button
          data-testid="next-button"
          handleClick={nextPage}
          text={<DoubleArrowRight fill="white" />}
          disabled={currentPage < end}
        />
      </ul>
    </div>
  );
};
Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  end: PropTypes.number.isRequired,
  changePage: PropTypes.func.isRequired,
  pagesCount: PropTypes.number.isRequired,
};
export default Pagination;
