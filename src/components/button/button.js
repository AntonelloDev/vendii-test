import PropTypes from 'prop-types';
import './button.scss';
import { addClasses } from '../../utils';
const Button = ({
  text,
  handleClick,
  type,
  selected,
  variant,
  customClasses,
  disabled,
}) => {
  const classes = addClasses([
    ...customClasses,
    type,
    variant,
    disabled && 'disabled',
  ]);
  return (
    <div className={`btn-container ${selected && 'selected'}`}>
      <button
        disabled={disabled}
        className={`btn ${selected && 'selected'} ${classes}`}
        onClick={(e) => handleClick(text, e)}
      >
        {text}
      </button>
    </div>
  );
};
Button.propTypes = {
  text: PropTypes.any.isRequired,
  handleClick: PropTypes.func.isRequired,
  selected: PropTypes.bool,
  type: PropTypes.string,
  variant: PropTypes.string,
  style: PropTypes.object,
  customClasses: PropTypes.array,
  disabled: PropTypes.bool,
};
Button.defaultProps = {
  type: 'round',
  selected: false,
  variant: 'primary',
  customClasses: [],
  disabled: false,
};
export default Button;
