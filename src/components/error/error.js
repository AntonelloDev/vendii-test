import './error.scss';
import PropTypes from 'prop-types';
const ErrorComponent = ({ message }) => {
  return (
    <div className="error-container">
      <div className="error">
        <p>{message}</p>
      </div>
    </div>
  );
};
ErrorComponent.propTypes = {
  message: PropTypes.string.isRequired,
};
export default ErrorComponent;
