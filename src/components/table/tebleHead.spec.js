import { render, act } from '@testing-library/react';
import TableHead from './TableHead';

describe('It should render correctly', () => {
  test('It should be defined', () => {
    act(() => {
      const comp = render(
        <table>
          <TableHead data={[]} />
        </table>
      );
      expect(comp).toBeDefined();
    });
  });
  test('It should render the props', () => {
    act(() => {
      const comp = render(
        <table>
          <TableHead data={[1, 2, 3, 4]} />
        </table>
      );
      expect(comp.getAllByTestId('th')).toHaveLength(4);
      expect(comp.getAllByTestId('th')[0]).toHaveTextContent(1);
      expect(comp.getAllByTestId('th')[1]).toHaveTextContent(2);
      expect(comp.getAllByTestId('th')[2]).toHaveTextContent(3);
      expect(comp.getAllByTestId('th')[3]).toHaveTextContent(4);
    });
  });
});
