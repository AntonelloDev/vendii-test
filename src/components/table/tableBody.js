import PropTypes from 'prop-types';
const TableBody = ({ data }) => (
  <tbody>
    {data.map((ele, i) => (
      <tr key={String(ele.url) + i} data-testid="table-row">
        {Object.values(ele).map((value, i) => {
          return (
            <td data-testid="td" key={String(value) + i}>
              {value}
            </td>
          );
        })}
      </tr>
    ))}
  </tbody>
);

TableBody.defaultProps = {
  data: PropTypes.array.isRequired,
};
TableBody.propTypes = {
  data: PropTypes.array.isRequired,
};
export default TableBody;
