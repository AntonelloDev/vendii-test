import TableBody from './tableBody';
import TableHead from './TableHead';
import './table.scss';
import PropTypes from 'prop-types';
const Table = ({ data }) => (
  <div className="tableWrapper">
    <table>
      <TableHead data={Object.keys(data[0])} />
      <TableBody data={data} />
    </table>
  </div>
);

Table.defaultProps = {
  data: [],
};
Table.propTypes = {
  data: PropTypes.array.isRequired,
};
export default Table;
