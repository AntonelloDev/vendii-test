import PropTypes from 'prop-types';
const TableHead = ({ data }) => {
  return (
    <thead>
      <tr data-testid="table-row">
        {data.map((ele) => (
          <th data-testid="th" key={ele}>
            {ele}
          </th>
        ))}
      </tr>
    </thead>
  );
};
TableHead.propTypes = {
  data: PropTypes.array.isRequired,
};
export default TableHead;
