import { render, act } from '@testing-library/react';
import TableBody from './TableBody';

describe('It should render correctly', () => {
  test('It should be defined', () => {
    act(() => {
      const comp = render(
        <table>
          <TableBody data={[]} />
        </table>
      );
      expect(comp).toBeDefined();
    });
  });
  test('It should render the props', () => {
    act(() => {
      const comp = render(
        <table>
          <TableBody data={[{ name: 'John', age: 22, favColor: 'red' }]} />
        </table>
      );
      expect(comp.getAllByTestId('table-row')).toHaveLength(1);
    });
  });
  test('', () => {
    const comp = render(
      <table>
        <TableBody data={[{ name: 'John', age: 22 }]} />
      </table>
    );
    expect(comp.getAllByTestId('td')).toHaveLength(2);
    expect(comp.getAllByTestId('td')[0]).toHaveTextContent('John');
    expect(comp.getAllByTestId('td')[1]).toHaveTextContent(22);
  });
});
