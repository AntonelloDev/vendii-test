import { render, act } from '@testing-library/react';
import App from './App';

describe('It should render correctly', () => {
  test('App should be defined', () => {
    act(() => {
      const appComp = render(<App />);
      expect(appComp).toBeDefined();
    });
  });
});
