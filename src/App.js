import { useState } from 'react';
import Pagination from './components/pagination/pagination';
import Table from './components/table/table';
import LoadingSpinner from './components/loading-spinner/loading-spinner';
import ErrorComponent from './components/error/error';
import { useData } from './customHooks';
const { REACT_APP_MAX_PER_PAGE: MAX_PER_PAGE } = process.env;
function App() {
  const [page, setPage] = useState(1);
  const [end, setEnd] = useState(MAX_PER_PAGE);
  const [direction, setDirection] = useState(null);
  /**
   *
   * @param {number} nextPage
   * @param {number} end
   * @param {string} direction
   */
  const changePage = (nextPage, end, direction) => {
    setPage(nextPage);
    setEnd(end);
    setDirection(direction);
  };

  const { data, isLoading, error, resultsCount } = useData(page, direction);
  if (isLoading) {
    return <LoadingSpinner />;
  }
  if (error) {
    return (
      <ErrorComponent message="something went wrong fetching the repos ..." />
    );
  }
  const maxPages = Math.floor(resultsCount / MAX_PER_PAGE);
  return (
    <div className="App">
      {data.length > 0 && <Table data={data} />}
      <Pagination
        maxPages={maxPages}
        currentPage={page}
        changePage={changePage}
        end={Number(end)}
        pagesCount={10}
      />
    </div>
  );
}
export default App;
