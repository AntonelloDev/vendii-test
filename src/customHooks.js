import { useState, useEffect } from 'react';
import API from './api/index';
import { delay, paginate, getNextSinceParam } from './utils';
const { REACT_APP_MAX_PER_PAGE: MAX_PER_PAGE } = process.env;
/**
 *
 * @param {number} page
 * @param {string} direction
 * @returns {object} { data, error, isLoading }
 */
export const useData = (page, direction) => {
  const [data, setData] = useState([]);
  const [responseData, setResponse] = useState([]);
  const [link, setLink] = useState('');
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(false);
  const [resultsCount, setResultsCount] = useState(0);
  const [requestCount, setRequestCount] = useState(0);
  const [visitedIds, setVisitedIds] = useState([1]);
  const [position, setPosition] = useState(0);
  useEffect(() => {
    if (requestCount > 0 && !direction) {
      const pageData = paginate(responseData, page, MAX_PER_PAGE);
      setData([...pageData]);
      setIsLoading(false);
    } else {
      return (async function () {
        try {
          let since;
          if (requestCount === 0) {
            since = 0;
            setRequestCount(1);
          }
          if (direction === 'r') {
            setPosition(position + 1);
            since = getNextSinceParam(link);
          } else if (direction === 'l') {
            since = visitedIds[position - 1] - 1;
            setPosition(position - 1);
          }
          setIsLoading(true);
          const start = Date.now();
          const response = await API.get('/repositories', { since });
          if (direction === 'r') {
            if (!visitedIds.includes(response?.data[0]?.id)) {
              setVisitedIds([...visitedIds, response?.data[0]?.id]);
            }
          }
          setLink(response?.headers?.link);
          setResultsCount(response?.data?.length);
          setResponse(response?.data);
          /**
           * Force a minimum loading time for consistency
           */
          const end = Date.now();
          const timeElapsed = end - start;
          if (timeElapsed < 400) {
            await delay(400 - timeElapsed);
          }
          const pageData = paginate(response?.data, page, MAX_PER_PAGE);
          setData([...pageData]);
          setIsLoading(false);
        } catch (error) {
          setError(true);
          setIsLoading(false);
        }
      })();
    }
  }, [page, direction]);
  return {
    data,
    isLoading,
    error,
    resultsCount,
  };
};
