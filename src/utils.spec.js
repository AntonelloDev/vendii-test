import {
  addClasses,
  selectInSchema,
  range,
  getNextSinceParam,
  paginate,
  findFirstElementIndex,
} from './utils';

describe('testing utils functions', () => {
  test('addClasses should take an array of strings and return a joined string ', () => {
    const data = ['one', 'two', 'three', 'four'];
    const expected = 'one two three four';
    const actual = addClasses(data);
    expect(actual).toEqual(expected);
  });
  test('it should filter out all props not in schema', () => {
    const data = {
      id: 1,
      full_name: 'Mike',
      age: 22,
      surname: 'Peterson',
      avatar_url: 'http://url.com',
      url: 'http://url-example.com',
      repos_url: 'http:repo-url.com',
    };
    const expected = {
      id: 1,
      full_name: 'Mike',
      url: 'http://url-example.com',
    };
    const actual = selectInSchema(data);
    expect(actual).toEqual(expected);
  });
  test('It should return an array given a start and an end', () => {
    const a = 10,
      b = 15;
    const expected = [10, 11, 12, 13, 14, 15];
    const actual = range(a, b);
    expect(actual).toEqual(expected);
  });
  test('Test the header link function that gets the next since param', () => {
    const data =
      '<https://api.github.com/repositories?since=370>; rel="next", <https://api.github.com/repositories{?since}>; rel="first"';
    const expected = '370';
    const result = getNextSinceParam(data);
    expect(result).toEqual(expected);
  });
});
describe('Testing Paginate function', () => {
  test('Test: 1', () => {
    const data = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
    const expected = [{ id: 2 }, { id: 3 }];
    const pageData = paginate(data, 1, 2);
    expect(pageData).toEqual(expected);
    const expectedCase2 = [{ id: 2 }, { id: 3 }];
    expect(paginate(data, 1, 2)).toEqual(expectedCase2);
  });
});

describe('testing fidFirstElement', () => {
  test('102 should return 2', () => {
    expect(findFirstElementIndex(102)).toEqual(2);
  });
  test('102 should return 2', () => {
    expect(findFirstElementIndex(109)).toEqual(9);
  });
  test('102 should return 2', () => {
    expect(findFirstElementIndex(120)).toEqual(20);
  });
  test('102 should return 2', () => {
    expect(findFirstElementIndex(160)).toEqual(60);
  });
  test('102 should return 2', () => {
    expect(findFirstElementIndex(111)).toEqual(11);
  });
  test('102 should return 2', () => {
    expect(findFirstElementIndex(140)).toEqual(40);
  });
});
